<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('currency');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
         $rules = ['amount' => ['required', 'numeric']];

        $v = Validator::make(Input::all(), $rules);

        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }
        else
        {       
            $amount =  $request->input('amount');
            $currency =  $request->input('currency');
            $contents = json_decode(file_get_contents("http://apilayer.net/api/live?access_key=4e19ea515863f010ee4a971e23b8597c&currencies={$currency}"));
            $currency =  'USD'.$currency;
            $value = $contents->quotes->$currency;
            $value = $value * $amount;                   
            return view('currency', compact('value'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
