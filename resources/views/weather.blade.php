<!DOCTYPE html>
<html>
    <head>
        <title>Weather</title>
    </head>
    <body>
    {{ $errors->first('city') }}
    {!! Form::open(array('method' => 'post')) !!}
    {!! Form::label('city', 'Please Enter city name.') !!}
    {!! Form::text('city', 'karachi, Pakistan') !!}
    {!! Form::submit('Check Weather') !!}
    {!! Form::close() !!}

    @if(isset($weather))
    <h3>Weather Today</h3>
        @foreach ($weather as $k => $v)
        <p>{{ $k }}: {{ $v }}</p>
        @endforeach
    @endif
    </body>
</html>
